package xodus.com.httpasync;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;
import android.view.SurfaceHolder;
import android.widget.RelativeLayout;


import android.view.View;
import android.widget.Toast;

/**
 * Created by Amir on 17-Nov-15.
 */
public class CustomButton extends RelativeLayout {

    View v;
    CustomTextView tvprice;
    Context c;


    public CustomButton(Context context) {
        super(context);
        this.c = context;
        tvprice = (CustomTextView) findViewById(R.id.cbprice);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.c = context;
        inflate(context,R.layout.custombutton,this);
        tvprice = (CustomTextView) findViewById(R.id.cbprice);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.c = context;
        inflate(context,R.layout.custombutton,this);
        tvprice = (CustomTextView) findViewById(R.id.cbprice);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.c = context;
        inflate(context,R.layout.custombutton,this);
        tvprice = (CustomTextView) findViewById(R.id.cbprice);
    }

    public void SetPrice(int price){
        tvprice.setText(price+"");
    }

}
