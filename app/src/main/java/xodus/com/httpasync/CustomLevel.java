package xodus.com.httpasync;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Amir on 17-Nov-15.
 */
public class CustomLevel extends RelativeLayout implements View.OnClickListener {

    GradientDrawable bgShape,bgitem;
    View bg, item1, item2, item3, item4, item5, item6, item7, item8,item;
    CustomTextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8;
    int selected,owned;

    public CustomLevel(Context context) {
        super(context);
        inflate(context, R.layout.customlevel, this);
    }

    public CustomLevel(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.customlevel, this);
    }

    public CustomLevel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.customlevel, this);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomLevel(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context, R.layout.customlevel, this);
    }

    public void SelectItem(int item) {

    }

    public void Init(int own, int select, int r, int g, int b) {
        bg = findViewById(R.id.itembg);
        bgShape = (GradientDrawable) bg.getBackground();
        bgShape.setColor(Color.rgb(r, g, b));
        item1 = findViewById(R.id.item1);
        item2 = findViewById(R.id.item2);
        item3 = findViewById(R.id.item3);
        item4 = findViewById(R.id.item4);
        item5 = findViewById(R.id.item5);
        item6 = findViewById(R.id.item6);
        item7 = findViewById(R.id.item7);
        item8 = findViewById(R.id.item8);
        tv1 = (CustomTextView) item1.findViewById(R.id.itemtext);
        tv2 = (CustomTextView) item2.findViewById(R.id.itemtext);
        tv3 = (CustomTextView) item3.findViewById(R.id.itemtext);
        tv4 = (CustomTextView) item4.findViewById(R.id.itemtext);
        tv5 = (CustomTextView) item5.findViewById(R.id.itemtext);
        tv6 = (CustomTextView) item6.findViewById(R.id.itemtext);
        tv7 = (CustomTextView) item7.findViewById(R.id.itemtext);
        tv8 = (CustomTextView) item8.findViewById(R.id.itemtext);

        item = item1.findViewById(R.id.shapeitem);
        bgitem = (GradientDrawable) item.getBackground();
        bgitem.setColor(Color.rgb(150, 150, 255));
        item = item2.findViewById(R.id.shapeitem);
        bgitem = (GradientDrawable) item.getBackground();
        bgitem.setColor(Color.rgb(150, 150, 255));
        item = item3.findViewById(R.id.shapeitem);
        bgitem = (GradientDrawable) item.getBackground();
        bgitem.setColor(Color.rgb(150, 150, 255));
        item = item4.findViewById(R.id.shapeitem);
        bgitem = (GradientDrawable) item.getBackground();
        bgitem.setColor(Color.rgb(150, 150, 255));
        item = item5.findViewById(R.id.shapeitem);
        bgitem = (GradientDrawable) item.getBackground();
        bgitem.setColor(Color.rgb(150, 150, 255));

        item1.setOnClickListener(this);
        item2.setOnClickListener(this);
        item3.setOnClickListener(this);
        item4.setOnClickListener(this);
        item5.setOnClickListener(this);
        item6.setOnClickListener(this);
        item7.setOnClickListener(this);
        item8.setOnClickListener(this);
        selected = select;
        owned = own;
        Check(selected);
    }

    @Override
    public void onClick(View v) {
        Uncheck(selected);
        switch (v.getId()) {
            case R.id.item1:
                selected = 1;
                break;
            case R.id.item2:
                if(owned>1)
                selected = 2;
                break;
            case R.id.item3:
                if(owned>2)
                selected = 3;
                break;
            case R.id.item4:
                if(owned>3)
                selected = 4;
                break;
            case R.id.item5:
                if(owned>4)
                selected = 5;
                break;
            case R.id.item6:
                if(owned>5)
                selected = 6;
                break;
            case R.id.item7:
                if(owned>6)
                selected = 7;
                break;
            case R.id.item8:
                if(owned>7)
                selected = 8;
                break;
        }
        Check(selected);
    }

    public void Uncheck(int i) {
        switch (i) {
            case 1:
                tv1.setVisibility(INVISIBLE);
                break;
            case 2:
                tv2.setVisibility(INVISIBLE);
                break;
            case 3:
                tv3.setVisibility(INVISIBLE);
                break;
            case 4:
                tv4.setVisibility(INVISIBLE);
                break;
            case 5:
                tv5.setVisibility(INVISIBLE);
                break;
            case 6:
                tv6.setVisibility(INVISIBLE);
                break;
            case 7:
                tv7.setVisibility(INVISIBLE);
                break;
            case 8:
                tv8.setVisibility(INVISIBLE);
                break;
        }
    }

    public void Check(int i) {
        switch (i) {
            case 1:
                tv1.setVisibility(VISIBLE);
                break;
            case 2:
                tv2.setVisibility(VISIBLE);
                break;
            case 3:
                tv3.setVisibility(VISIBLE);
                break;
            case 4:
                tv4.setVisibility(VISIBLE);
                break;
            case 5:
                tv5.setVisibility(VISIBLE);
                break;
            case 6:
                tv6.setVisibility(VISIBLE);
                break;
            case 7:
                tv7.setVisibility(VISIBLE);
                break;
            case 8:
                tv8.setVisibility(VISIBLE);
                break;
        }
    }
}
