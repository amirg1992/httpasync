package xodus.com.httpasync;

/**
 * Created by Amir on 15-Nov-15.
 */

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;


public class Design extends ActionBarActivity {

    private Toolbar tb;
    private TextView tvcash, tvcoin;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.design);
        init();
        initToolbar();
        List<SetGetDesign> itemlist = new ArrayList<>();
        SetGetDesign item = new SetGetDesign();
        item.InitDesign(8, 9900, 2, 5,0,0,150, "مربی تیم");
        itemlist.add(item);
        DesignListAdapter adaper = new DesignListAdapter(Design.this, this, itemlist);
        list.setAdapter(adaper);
        //View shape = findViewById(R.id.rowshapeitem1);
        //View v = shape.findViewById(R.id.shapeitem);
        //GradientDrawable bgShape = (GradientDrawable) v.getBackground();
        //bgShape.setColor(Color.rgb(255,0,0));
    }


    private void init() {
        tb = (Toolbar) findViewById(R.id.app_bar);
        tvcash = (TextView) findViewById(R.id.descash);
        tvcoin = (TextView) findViewById(R.id.descoin);
        list = (ListView) findViewById(R.id.deslist);
    }

    private void initToolbar() {
        setSupportActionBar(tb);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        TextView tvtitle = (TextView) findViewById(R.id.tbtitle);
        tvtitle.setText("کادر تیم");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                NavUtils.navigateUpFromSameTask(Design.this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
