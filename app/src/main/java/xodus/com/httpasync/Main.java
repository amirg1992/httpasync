package xodus.com.httpasync;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.extras.Base64;

public class Main extends AppCompatActivity {

    private String URL = "https://mandrillapp.com/api/1.0/messages/send.json";
    private String response = null;
    private Button bsend,bdesign;
    private EditText etmessage, etmail;
    private TextView tvresult;
    private String KEY =  "Ldjfk4EThOpYHA4YvgBcXA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Toolbar tb = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(tb);
        getSupportActionBar().setTitle("");
        TextView tvtitle = (TextView) findViewById(R.id.tbtitle);
        tvtitle.setText("Email");
        bsend  = (Button) findViewById(R.id.bsend);
        etmessage = (EditText) findViewById(R.id.etmessage);
        etmail = (EditText) findViewById(R.id.etmail);
        tvresult = (TextView) findViewById(R.id.tvresult);
        bsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jobjmain = new JSONObject();
                JSONObject jobmessage = new JSONObject();
                JSONArray jarrto = new JSONArray();
                JSONObject jobemail = new JSONObject();
                JSONArray jarattach = new JSONArray();
                JSONObject jobattach = new JSONObject();

                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image);
                try {
                    jobattach.put("type", "image/png");
                    jobattach.put("name", "image.png");
                    jobattach.put("content", encodeTobase64(bm));
                    jarattach.put(jobattach);

                    jobemail.put("email",etmail.getText().toString());
                    jarrto.put(jobemail);

                    jobmessage.put("text",etmessage.getText().toString());
                    jobmessage.put("from_email","NoBody@xodus.com");
                    jobmessage.put("to",jarrto);
                    jobmessage.put("attachments",jarattach);

                    jobjmain.put("key", "eOe08PhjtGMqOZoABJSmhA");
                    jobjmain.put("message", jobmessage);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //tvresult.setText(jobjmain.toString());
                MakeRequest(jobjmain);
            }
        });
        bdesign = (Button) findViewById(R.id.bdesign);
        bdesign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main.this,Design.class);
                startActivity(i);
            }
        });

    }

    private void MakeRequest(JSONObject obj) {
        tvresult.setText(obj.toString());
        StringEntity entity = null;
        try {
            entity = new StringEntity(obj.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Main.this, URL, entity, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    response = new String(responseBody, "UTF-8");
                    tvresult.setText("Result:\n" + response);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    response = new String(responseBody, "UTF-8");
                    tvresult.setText(tvresult.getText() + "\n\n" + response);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    public static String encodeTobase64(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.design:
                Intent i = new Intent(Main.this,Design.class);
                startActivity(i);
                break;
            case R.id.action_settings:
                Toast.makeText(Main.this,"Settings Selected",Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
