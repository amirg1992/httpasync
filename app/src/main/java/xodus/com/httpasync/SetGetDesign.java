package xodus.com.httpasync;

/**
 * Created by Amir on 17-Nov-15.
 */
public class SetGetDesign {
    private int Level, Price, Selected, Owned, R, G, B;
    private String Title;

    public SetGetDesign() {
    }

    public void InitDesign(int level, int price, int selected, int owned, int r, int g, int b, String title) {
        this.Level = level;
        this.Price = price;
        this.Selected = selected;
        this.Owned = owned;
        this.R = r;
        this.G = g;
        this.B = b;
        this.Title = title;
    }

    //Set
    public void SetLevel(int level) {
        this.Level = level;
    }

    public void SetPrice(int price) {
        this.Price = price;
    }

    public void SetSelected(int selected) {
        this.Selected = selected;
    }

    public void SetOwned(int owned) {
        this.Owned = owned;
    }

    public void SetR(int r) {
        this.R = r;
    }

    public void SetG(int g) {
        this.G = g;
    }

    public void SetB(int b) {
        this.B = b;
    }

    public void SetTitle(String title) {
        this.Title = title;
    }

    //Get

    public int GetLevel() {
        return this.Level;
    }

    public int GetPrice() {
        return this.Price;
    }

    public int GetSelected() {
        return this.Selected;
    }

    public int GetOwned() {
        return this.Owned;
    }

    public int GetR() {
        return this.R;
    }

    public int GetG() {
        return this.G;
    }

    public int GetB() {
        return this.B;
    }

    public String GetTitle() {
        return this.Title;
    }


}
