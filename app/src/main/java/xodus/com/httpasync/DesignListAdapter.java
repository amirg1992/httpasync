package xodus.com.httpasync;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.widget.Toast;
import android.app.Activity;

import java.util.List;

/**
 * Created by Amir on 17-Nov-15.
 */
public class DesignListAdapter extends BaseAdapter {

    private Context context;
    private List<SetGetDesign> Items;
    private LayoutInflater layinf = null;
    private Activity activity;


    public DesignListAdapter(Context c, Activity act, List<SetGetDesign> items) {
        this.context = c;
        this.Items = items;
        this.activity = act;
        layinf = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ListCell cell;

        if (convertView == null) {
            convertView = layinf.inflate(R.layout.list_row, null);
            cell = new ListCell();
            cell.ibinfo = (ImageButton) convertView.findViewById(R.id.rowinfo);

            cell.tvtitle = (CustomTextView) convertView.findViewById(R.id.rowtvtitle);

            cell.tvlevel = (CustomTextView) convertView.findViewById(R.id.rowtvlevel);

            cell.cbprice = (CustomButton) convertView.findViewById(R.id.custombutton);

            cell.clevel = (CustomLevel) convertView.findViewById(R.id.rowlevel);

            cell.tvnext = (CustomTextView) convertView.findViewById(R.id.rowtvnext);

            // cell.vitems = convertView.findViewById(R.id.rowlevel);
            //cell.vitembg = cell.vitems.findViewById(R.id.itembg);

            convertView.setTag(cell);
        } else {
            cell = (ListCell) convertView.getTag();
        }

        int level = Items.get(position).GetLevel();

        cell.tvtitle.setText(Items.get(position).GetTitle());

        cell.tvlevel.setText(level + "");

        cell.cbprice.SetPrice(Items.get(position).GetPrice());

        cell.clevel.Init(Items.get(position).GetOwned(), Items.get(position).GetSelected(), Items.get(position).GetR(), Items.get(position).GetG(), Items.get(position).GetB());

        int temp = level +1;
        cell.tvnext.setText("ارتقا به سطح " + temp + ":");

        //GradientDrawable bgShape = (GradientDrawable) cell.vitembg.getBackground();
        //bgShape.setColor(Color.rgb(Items.get(position).GetR(),Items.get(position).GetG(),Items.get(position).GetB()));


        cell.ibinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, Items.get(position).GetTitle(), Toast.LENGTH_SHORT).show();
            }
        });

        cell.cbprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Price is " + Items.get(position).GetPrice(), Toast.LENGTH_SHORT).show();

            }
        });

        return convertView;
    }

    private class ListCell {
        ImageButton ibinfo;
        xodus.com.httpasync.CustomTextView tvtitle, tvlevel, tvnext;
        CustomButton cbprice;
        CustomLevel clevel;

    }
}
